import axios from "axios";

export default axios.create({
  baseURL: "https://nodejs-express-mysql-rest.herokuapp.com/api",
  headers: {
    "Content-type": "application/json"
  }
});